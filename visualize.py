#!/usr/bin/env python
#
# File: visualize.py
#
# Created: Friday, May 13 2016 by rejuvyesh <mail@rejuvyesh.com>
# License: GNU GPL 3 <http://www.gnu.org/copyleft/gpl.html>
#
import os              
import glob
import uuid
import scipy.misc
import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from data_loader import get_video_annotations, annomat

def visualize(img_dir, anno):
  imfiles = sorted(glob.glob(os.path.join(img_dir, '*.jpg')))
  vidanno = get_video_annotations(anno)
  fig = plt.figure()
  ax = fig.add_subplot(111)
  for i in range(len(imfiles)//2+3, len(imfiles), 3):
    im = scipy.misc.imread(imfiles[i])
    ax.imshow(im)
    for j in range(len(vidanno[i].tid)):
      idx = np.mod(vidanno[i].tid[j]*10, 64)
      ax.add_patch(patches.Rectangle(vidanno[i].bbox[j][:2],
                                     vidanno[i].bbox[j][2],
                                     vidanno[i].bbox[j][3], linewidth=2, fill=False))
    plt.savefig('data2/{}.png'.format(uuid.uuid4()))
    plt.show()

if __name__ == '__main__':
  num = sys.argv[1]  
  anno = annomat('data2/annotations/anno{}.mat'.format(num))
  visualize('data2/images/seq{}'.format(num), anno)
