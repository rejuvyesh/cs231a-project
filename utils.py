#
# File: utils.py
#
# Created: Friday, May 13 2016 by rejuvyesh <mail@rejuvyesh.com>
# License: GNU GPL 3 <http://www.gnu.org/copyleft/gpl.html>
#
class Colors(object):
  HEADER = '\033[95m'
  OKBLUE = '\033[94m'
  OKGREEN = '\033[92m'
  WARNING = '\033[93m'
  FAIL = '\033[91m'
  ENDC = '\033[0m'
def header(s): print(Colors.HEADER + s + Colors.ENDC)
def warn(s): print(Colors.WARNING + s + Colors.ENDC)
def failure(s): print(Colors.FAIL + s + Colors.ENDC)
def okb(s): print(Colors.OKBLUE + s + Colors.ENDC)
def ok(s): print(Colors.OKGREEN + s + Colors.ENDC)
