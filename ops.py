#
# File: ops.py
#
# Created: Wednesday, May 11 2016 by rejuvyesh <mail@rejuvyesh.com>
# License: GNU GPL 3 <http://www.gnu.org/copyleft/gpl.html>
#
import numpy as np
import tensorflow as tf
from tensorflow.models.rnn import rnn  # FIXME

from base import Model
from data_loader import DataLoader
from utils import *


def conv(input_, kernel, biases, k_h, k_w, c_o, s_h, s_w, padding='VALID', group=1, name="conv"):
  c_i = input_.get_shape()[-1]
  assert c_i%group == 0
  assert c_o%group == 0
  convolve = lambda i, k: tf.nn.conv2d(input=i, filter=k, strides=[1, s_h, s_w, 1], padding=padding, name=name)
  if group == 1:
    conv = convolve(input_, kernel)
  else:
    input_groups = tf.split(3, group, input_)
    kernel_groups = tf.split(3, group, kernel)
    output_groups = [convolve(i,k) for i,k in zip(input_groups, kernel_groups)]
    conv = tf.concat(3, output_groups)
  return tf.reshape(tf.nn.bias_add(conv, biases), conv.get_shape().as_list())


class CNN(Model):
  def __init__(self, input_, init_weights):
    #conv1
    #conv(11, 11, 96, 4, 4, padding='VALID', name='conv1')
    print("input {}".format(input_.get_shape()))
    k_h = 11; k_w = 11; c_o = 96; s_h = 4; s_w = 4
    conv1W = tf.Variable(init_weights["conv1"][0])
    conv1b = tf.Variable(init_weights["conv1"][1])
    conv1_in = conv(input_, conv1W, conv1b,
                    k_h, k_w, c_o, s_h, s_w, padding="SAME", group=1)
    conv1 = tf.nn.relu(conv1_in, name="conv1")

    #lrn1
    #lrn(2, 2e-05, 0.75, name='norm1')
    radius = 2; alpha = 2e-05; beta = 0.75; bias = 1.0
    lrn1 = tf.nn.local_response_normalization(conv1,
                                              depth_radius=radius,
                                              alpha=alpha,
                                              beta=beta,
                                              bias=bias)

    #maxpool1
    #max_pool(3, 3, 2, 2, padding='VALID', name='pool1')
    k_h = 3; k_w = 3; s_h = 2; s_w = 2; padding = 'VALID'
    maxpool1 = tf.nn.max_pool(lrn1, ksize=[1, k_h, k_w, 1], strides=[1, s_h, s_w, 1], padding=padding)

    #conv2
    #conv(5, 5, 256, 1, 1, group=2, name='conv2')
    k_h = 5; k_w = 5; c_o = 256; s_h = 1; s_w = 1; group = 2
    conv2W = tf.Variable(init_weights["conv2"][0])
    conv2b = tf.Variable(init_weights["conv2"][1])
    conv2_in = conv(maxpool1, conv2W, conv2b,
                    k_h, k_w, c_o, s_h, s_w, padding="SAME", group=group)
    conv2 = tf.nn.relu(conv2_in, name="conv2")

    #lrn2
    #lrn(2, 2e-05, 0.75, name='norm2')
    radius = 2; alpha = 2e-05; beta = 0.75; bias = 1.0
    lrn2 = tf.nn.local_response_normalization(conv2,
                                              depth_radius=radius,
                                              alpha=alpha,
                                              beta=beta,
                                              bias=bias)

    #maxpool2
    #max_pool(3, 3, 2, 2, padding='VALID', name='pool2')
    k_h = 3; k_w = 3; s_h = 2; s_w = 2; padding = 'VALID'
    maxpool2 = tf.nn.max_pool(lrn2, ksize=[1, k_h, k_w, 1], strides=[1, s_h, s_w, 1], padding=padding)

    #conv3
    #conv(3, 3, 384, 1, 1, name='conv3')
    k_h = 3; k_w = 3; c_o = 384; s_h = 1; s_w = 1; group = 1
    conv3W = tf.Variable(init_weights["conv3"][0])
    conv3b = tf.Variable(init_weights["conv3"][1])
    conv3_in = conv(maxpool2, conv3W, conv3b,
                    k_h, k_w, c_o, s_h, s_w, padding="SAME", group=group)
    conv3 = tf.nn.relu(conv3_in, name="conv3")

    #conv4
    #conv(3, 3, 384, 1, 1, group=2, name='conv4')
    k_h = 3; k_w = 3; c_o = 384; s_h = 1; s_w = 1; group = 2
    conv4W = tf.Variable(init_weights["conv4"][0])
    conv4b = tf.Variable(init_weights["conv4"][1])
    conv4_in = conv(conv3, conv4W, conv4b,
                    k_h, k_w, c_o, s_h, s_w, padding="SAME", group=group)
    conv4 = tf.nn.relu(conv4_in, name="conv4")

    #conv5
    #conv(3, 3, 256, 1, 1, group=2, name='conv5')
    k_h = 3; k_w = 3; c_o = 256; s_h = 1; s_w = 1; group = 2
    conv5W = tf.Variable(init_weights["conv5"][0])
    conv5b = tf.Variable(init_weights["conv5"][1])
    conv5_in = conv(conv4, conv5W, conv5b,
                    k_h, k_w, c_o, s_h, s_w, padding="SAME", group=group)
    conv5 = tf.nn.relu(conv5_in, name="conv5")

    #maxpool5
    #max_pool(3, 3, 2, 2, padding='VALID', name='pool5')
    k_h = 3; k_w = 3; s_h = 2; s_w = 2; padding = 'VALID'
    maxpool5 = tf.nn.max_pool(conv5, ksize=[1, k_h, k_w, 1], strides=[1, s_h, s_w, 1], padding=padding)

    #fc6
    #fc(4096, name='fc6')
    fc6W = tf.Variable(init_weights["fc6"][0])
    fc6b = tf.Variable(init_weights["fc6"][1])
    fc6 = tf.nn.relu_layer(tf.reshape(maxpool5, [input_.get_shape().as_list()[0], int(np.prod(maxpool5.get_shape().as_list()[1:]))]), fc6W, fc6b, name="fc6")

    #fc7
    #fc(4096, name='fc7')
    fc7W = tf.Variable(init_weights["fc7"][0])
    fc7b = tf.Variable(init_weights["fc7"][1])
    fc7 = tf.nn.relu_layer(fc6, fc7W, fc7b, name="fc7")

    self.output = fc7

class SimpleActNet(Model):
  def __init__(self, sess, cnn_weights, n_classes=3, batch_size=4):
    self.sess = sess
    self.batch_size = batch_size
    self.dataset_name = dataset_name

    self.img_size = 227
    self.n_classes = n_classes

    self.data_dir = data_dir
    self.checkpoint_dir = checkpoint_dir

    self.loader = DataLoader(self.data_dir, self.batch_size, self.n_steps)
    self.prepare_model(cnn_weights)


  def prepare_model(self, cnn_weights):
    with tf.variable_scope("SimpActNet"):
      with tf.variable_scope("CNN") as scope:
        self.img_inputs = tf.placeholder(tf.float32, [self.batch_size*self.n_steps, self.img_size, self.img_size, 3])
        cnn = CNN(self.img_inputs, cnn_weights)
        self.cnn_outputs = cnn.output
        self.true_outputs = tf.placeholder(tf.float32, [self.batch_size, self.n_classes])
        self.loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(proj, self.true_outputs))
        tf.scalar_summary("loss", self.loss)

  def train(self, epoch, mode=2):
    cost = 0
    target = np.zeros([self.batch_size, self.n_classes])
    N = self.loader.sizes[0]
    for idx in range(N):
      target.fill(0)
      x, y = self.loader.next_batch(0, mode=mode)
      for b in range(self.batch_size):
        if y[b, -1] >= 0:
          # for c in range(self.n_classes):
          #   if c in y[:,-1]:
          target[b][y[b,-1]] = 1
      feed_dict = {
        self.img_inputs  : x,
        self.true_outputs : target
      }
      _, loss, summary_str = self.sess.run([self.optimizer, self.loss, self.merged_summary],
                                           feed_dict=feed_dict)
      self.writer.add_summary(summary_str)
      okb("epoch: %2d loss: %2.6f" %(epoch, loss))
      cost += loss
    return cost / N

  def test(self, split_idx, mode=1, max_batches=None):
    N = self.loader.sizes[split_idx]
    if max_batches != None:
      N = min(max_batches, N)
      target = np.zeros([self.batch_size, self.n_classes])
      self.loader.reset_batch_pointer(split_idx)

    cost = 0
    for idx in range(N):
      target.fill(0)
      x, y = self.loader.next_batch(split_idx, mode=mode)
      for b in range(self.batch_size):
        if y[b,-1] >= 0:
          # for t,w in enumerate(y[b,-1]):
          #   if w >= 0:
          target[b][y[b,-1]] = 1

      feed_dict = {
        self.img_inputs : x,
        self.true_outputs : target
      }
      loss = self.sess.run(self.loss, feed_dict=feed_dict)
      ok("    > Valid loss: %2.6f" % loss)
      cost += loss
      cost = cost / N
    return cost

  def run(self, epoch=200, learning_rate=0.01, mode=1):
    self.learning_rate = learning_rate
    self.optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate).minimize(self.loss)

    tf.initialize_all_variables().run()
    if self.load(self.checkpoint_dir, self.dataset_name):
      ok(" [*] SUCCESS to load model for %s." % self.dataset_name)
    else:
      warn(" [!] Failed to load model for %s." % self.dataset_name)

    self.saver = tf.train.Saver()
    self.merged_summary = tf.merge_all_summaries()
    self.writer = tf.train.SummaryWriter("./logs", self.sess.graph)

    self.log_loss = []
    for idx in range(epoch):
      train_loss = self.train(idx, mode=mode)
      val_loss = self.test(1, mode=mode)

      self.log_loss.append([train_loss, val_loss])

      state = {
        'epoch': idx,
        'train loss': train_loss,
        'val_loss': val_loss
      }
      print(state)
      if idx % 2 == 0:
        self.save(self.checkpoint_dir, self.dataset_name)
  

class ActNet(Model):
  def __init__(self, sess, cnn_weights, n_classes=3, batch_size=4, rnn_size=3000, n_steps=9, checkpoint_dir="checkpoint", data_dir="data", dataset_name="choi"):
    self.sess = sess
    self.batch_size = batch_size
    self.dataset_name = dataset_name

    self.img_size = 227

    # LSTM
    self.rnn_size = rnn_size
    self.n_steps = n_steps

    self.n_classes = n_classes

    self.data_dir = data_dir
    self.checkpoint_dir = checkpoint_dir

    self.loader = DataLoader(self.data_dir, self.batch_size, self.n_steps)
    self.prepare_model(cnn_weights)

  def prepare_model(self, cnn_weights):
    with tf.variable_scope("ActNet"):
      with tf.variable_scope("CNN") as scope:
        self.img_inputs = tf.placeholder(tf.float32, [self.batch_size*self.n_steps, self.img_size, self.img_size, 3])
        cnn = CNN(self.img_inputs, cnn_weights)
        self.cnn_outputs = cnn.output
        print('cnn {}'.format(self.cnn_outputs.get_shape()))

      with tf.variable_scope("LSTM1") as scope:
        lstm_weights = {
          'hidden': tf.Variable(tf.random_normal([self.cnn_outputs.get_shape().as_list()[-1], self.rnn_size])), # Hidden layer weights TODO
          'out': tf.Variable(tf.random_normal([self.rnn_size, self.n_classes]))
        }
        lstm_biases = {
          'hidden': tf.Variable(tf.random_normal([self.rnn_size])),
          'out': tf.Variable(tf.random_normal([self.n_classes]))
        }
        self.lstm_cell = tf.nn.rnn_cell.BasicLSTMCell(self.rnn_size)
        # TODO
        _lstm_input = tf.matmul(self.cnn_outputs, lstm_weights['hidden']) + lstm_biases['hidden']
        self.lstm_input = tf.split(0, self.n_steps, self.cnn_outputs, name='LSTM1In')

        self.true_outputs = tf.placeholder(tf.float32, [self.batch_size, self.n_classes])
        outputs, states = rnn.rnn(self.lstm_cell, self.lstm_input, dtype=tf.float32)
        print('outputs lstm1: {}'.format(len(outputs)))
        proj = tf.matmul(outputs[-1], lstm_weights['out']) + lstm_biases['out']
        # TODO: Should we use all outputs?
        self.loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(proj, self.true_outputs))
        tf.scalar_summary("loss", self.loss)

  def train(self, epoch, mode=1):
    cost = 0
    target = np.zeros([self.batch_size, self.n_classes])
    N = self.loader.sizes[0]
    for idx in range(N):
      target.fill(0)
      x, y = self.loader.next_batch(0, mode=mode)
      for b in range(self.batch_size):
        if y[b, -1] >= 0:
        # for c in range(self.n_classes):
        #   if c in y[:,-1]:
          target[b][y[b,-1]] = 1
      feed_dict = {
        self.img_inputs  : x,
        self.true_outputs : target
      }
      _, loss, summary_str = self.sess.run([self.optimizer, self.loss, self.merged_summary],
                                         feed_dict=feed_dict)
      self.writer.add_summary(summary_str)
      okb("epoch: %2d loss: %2.6f" %(epoch, loss))
      cost += loss
    return cost / N

  def test(self, split_idx, mode=1, max_batches=None):
    N = self.loader.sizes[split_idx]
    if max_batches != None:
      N = min(max_batches, N)
    target = np.zeros([self.batch_size, self.n_classes])
    self.loader.reset_batch_pointer(split_idx)

    cost = 0
    for idx in range(N):
      target.fill(0)
      x, y = self.loader.next_batch(split_idx, mode=mode)
      for b in range(self.batch_size):
        if y[b,-1] >= 0:
        # for t,w in enumerate(y[b,-1]):
        #   if w >= 0:
          target[b][y[b,-1]] = 1

      feed_dict = {
        self.img_inputs : x,
        self.true_outputs : target
      }
      loss = self.sess.run(self.loss, feed_dict=feed_dict)
      ok("    > Valid loss: %2.6f" % loss)
      cost += loss
    cost = cost / N
    return cost

  def run(self, epoch=2, learning_rate=0.01, mode=1):
    self.learning_rate = learning_rate
    self.optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate).minimize(self.loss)

    tf.initialize_all_variables().run()
    if self.load(self.checkpoint_dir, self.dataset_name):
      ok(" [*] SUCCESS to load model for %s." % self.dataset_name)
    else:
      warn(" [!] Failed to load model for %s." % self.dataset_name)

    self.saver = tf.train.Saver()
    self.merged_summary = tf.merge_all_summaries()
    self.writer = tf.train.SummaryWriter("./logs", self.sess.graph)

    self.log_loss = []
    for idx in range(epoch):
      train_loss = self.train(idx, mode=mode)
      val_loss = self.test(1, mode=mode)

      self.log_loss.append([train_loss, val_loss])

      state = {
        'epoch': idx,
        'train loss': train_loss,
        'val_loss': val_loss
      }
      print(state)
      if idx % 2 == 0:
        self.save(self.checkpoint_dir, self.dataset_name)

class GroupActNet(ActNet):
  def __init__(self, sess, cnn_weights, n_classes=6, batch_size=4, rnn_size=3000, n_steps=9, checkpoint_dir="checkpoint", data_dir="data", dataset_name="choi"):
    self.sess = sess
    self.batch_size = batch_size
    self.dataset_name = dataset_name

    self.img_size = 227

    # LSTM
    self.rnn_size = rnn_size
    self.n_steps = n_steps

    self.n_classes = n_classes

    self.data_dir = data_dir
    self.checkpoint_dir = checkpoint_dir

    self.loader = DataLoader(self.data_dir, self.batch_size, self.n_steps)
    self.prepare_model(cnn_weights)

  def prepare_model(self, cnn_weights):
    with tf.variable_scope("ActNet"):
      with tf.variable_scope("CNN") as scope:
        self.img_inputs = tf.placeholder(tf.float32, [self.batch_size*self.n_steps, self.img_size, self.img_size, 3])
        cnn = CNN(self.img_inputs, cnn_weights)
        self.cnn_outputs = cnn.output
        print('cnn {}'.format(self.cnn_outputs.get_shape()))

      with tf.variable_scope("LSTM1") as scope:
        lstm_weights = {
          'hidden': tf.Variable(tf.random_normal([self.cnn_outputs.get_shape().as_list()[-1], self.rnn_size])), # Hidden layer weights TODO
          'out': tf.Variable(tf.random_normal([self.rnn_size, self.n_classes]))
        }
        lstm_biases = {
          'hidden': tf.Variable(tf.random_normal([self.rnn_size])),
          'out': tf.Variable(tf.random_normal([self.n_classes]))
        }
        self.lstm1_cell = tf.nn.rnn_cell.BasicLSTMCell(self.rnn_size)
        # TODO
        _lstm1_input = tf.matmul(self.cnn_outputs, lstm_weights['hidden']) + lstm_biases['hidden']
        self.lstm1_input = tf.split(0, self.n_steps, _lstm1_input, name='LSTM1In')


        self.lstm1_outputs, states = rnn.rnn(self.lstm1_cell, self.lstm1_input, dtype=tf.float32)

        print('outputs lstm1: {}'.format(len(self.lstm1_outputs)))

      with tf.variable_scope("POOL") as scope:
        self.pool_inputs = tf.concat(0, [self.lstm1_outputs, self.cnn_outputs])
        self.pool_outputs = tf.nn.max_pool(self.pool_inputs, ksize=[1, 1, 1, 1], strides=[1, 1, 1, 1], padding=padding)

      with tf.variable_scope("LSTM2") as scope:
        lstm2_weights = {
          'hidden': tf.Variable(tf.random_normal([self.pool_outputs.get_shape().as_list()[-1], self.rnn_size])), # Hidden layer weights TODO
          'out': tf.Variable(tf.random_normal([self.rnn_size, self.n_classes]))
        }
        lstm2_biases = {
          'hidden': tf.Variable(tf.random_normal([self.rnn_size])),
          'out': tf.Variable(tf.random_normal([self.n_classes]))
        }
        self.lstm2_cell = tf.nn.rnn_cell.BasicLSTMCell(self.rnn_size)
        _lstm2_input = tf.matmul(self.pool_outputs, lstm2_weights['hidden']) + lstm2_biases['hidden']
        self.lstm2_input = tf.split(0, self.nsteps, _lstm2_input, name='LSTM2In')
        self.lstm2_output, l2_states = rnn.rnn(self.lstm2_cell, self.lstm2_input, dtype=tf.float32)

        print('outputs lstm2: {}'.format(len(self.lstm2_outputs)))

        self.proj = tf.matmul(self.lstm2_outputs[-1], lstm2_weights['out']) + lstm2_biases['out']
        self.true_outputs = tf.placeholder(tf.float32, [self.batch_size, self.n_classes])

        self.loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(self.proj, self.true_outputs))
        tf.scalar_summary("loss", self.loss)
