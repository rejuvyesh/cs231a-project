#!/usr/bin/env python
#
# File: main.py
#
# Created: Thursday, May 12 2016 by rejuvyesh <mail@rejuvyesh.com>
# License: GNU GPL 3 <http://www.gnu.org/copyleft/gpl.html>
#
import os
import numpy as np
import scipy.misc
import tensorflow as tf

from ops import ActNet

def main():
  with tf.Session() as sess:
    cnn_weights = np.load("bvlc_alexnet.npy").item()
    model = ActNet(sess, cnn_weights)
    model.run(mode=2)
    
if __name__ == '__main__':
  main()
