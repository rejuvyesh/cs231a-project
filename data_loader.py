#
# File: data_loader.py
#
# Created: Wednesday, May 11 2016 by rejuvyesh <mail@rejuvyesh.com>
# License: GNU GPL 3 <http://www.gnu.org/copyleft/gpl.html>
#
import scipy.misc
import glob
import os
from collections import defaultdict
import numpy as np
import scipy.io
import pickle
from utils import *

class VideoAnnotation(object):
  """
  """

  def __init__(self, clabel):
    """

    Arguments:
    - `clabel`:
    - `tid`:
    - `bbox`:
    - `actions`:
    - `poses`:
    - `interactions`:
    - `ilines`:
    """
    self.clabel = clabel
    self.tid = []
    self.bbox = []
    self.actions = []
    self.poses = []
    self.interactions = []
    self.ilines = []

  def __len__(self):
    return len(self.bbox)


def get_video_annotations(anno):
  """

  Args:
  anno Annotation dict

  Returns:


  """

  cstr = ['NA', 'Gathering', 'Talking', 'Dismissal', 'Walking together', 'Chasing', 'Queuing'] # Collective Activity
  astr = ['S','W','R']          # Atomic activity
  istr = ['NA','AP','WO','WS','WR','RS','RR','FE','SR'] # Interaction
  pstr = ['R','RF','F','LF','L','LB','B','RB'] # Pose ID

  vidanno = []

  for j in range(anno['nframe']):
    clabel = anno['collective'][j]
    vidanno.append(VideoAnnotation(clabel-1))

  for i in range(len(anno['people'][0])):
    for j in range(len(anno['people'][0][i]['time'][0])):
      fr = anno['people'][0][i]['time'][0][j]-1
      assert fr >= 0 and fr < anno['nframe'], fr

      vidanno[fr].tid.append(i)
      vidanno[fr].bbox.append(anno['people'][0][i]['sbbs'][:,j])

      if anno['people'][0][i]['attr'][0,j] <= 0:
        vidanno[fr].poses.append(-10)
      else:
        vidanno[fr].poses.append(pstr[anno['people'][0][i]['attr'][0,j]-1])

      if anno['people'][0][i]['attr'][1,j] <= 0:
        vidanno[fr].actions.append(-10) # TODO: NA handling
      else:
        # vidanno[fr].actions.append(astr[anno['people'][0][i]['attr'][1,j]-1])
        vidanno[fr].actions.append(anno['people'][0][i]['attr'][1,j]-1)

  for f, va in enumerate(vidanno):
    assert len(va.tid) <= len(anno['people'][0]), (len(va.tid), len(anno['people'][0]), f)

  for i in range(len(anno['people'][0])):
    for j in range(i+1,len(anno['people'][0])):
      frames = np.intersect1d(anno['people'][0][i]['time'][0], anno['people'][0][j]['time'][0])

      iid = get_interaction_idx(i+1, j+1, len(anno['people'][0]))

      for t in range(len(frames)):
        fr = frames[t]-1

        if anno['interaction'][iid,fr] <= 1:
          continue
        else:
          vidanno[fr].interactions.append(anno['interaction'][iid, fr]-1)

        bb1 = anno['people'][0][i]['sbbs'][:, anno['people'][0][i]['time'][0] == fr]
        bb2 = anno['people'][0][j]['sbbs'][:, anno['people'][0][j]['time'][0] == fr]

        vidanno[fr].ilines.append([bb1[0]+bb1[2]/2, bb1[1]+bb1[3], bb2[0]+bb2[2]/2, bb2[1]+bb2[3]])

  return vidanno

def get_interaction_idx(a1, a2, na):
  assert a1 > 0, a1
  assert a2 > 0, a2
  assert a1 <= na, (a1, na)
  assert a2 <= na, (a2, na)
  assert a1 != a2, (a1, a2)
  return get_pair_idx(np.min([a1, a2]), np.max([a1, a2])-1, na-1)


def get_pair_idx(min_idx, max_idx, num_label):
  idx = ((num_label*(num_label+1))//2
         - ((num_label-min_idx+1)*(num_label-min_idx+2))//2
         + (max_idx-min_idx)) #+1?
  return idx

# FIXME
# See https://github.com/tensorflow/tensorflow/issues/1541
# See http://stackoverflow.com/questions/16612293/scipy-misc-imread-creates-an-image-with-no-size-or-shape
def imread(path):
  return scipy.misc.imread(path).astype(np.float32)

def annomat(path):
  dat = scipy.io.loadmat(path)['anno']
  res = {
    'nframe' : dat['nframe'][0][0][0][0],
    'people' : dat['people'][0][0],
    'interaction' : dat['interaction'][0][0],
    'collective' : dat['collective'][0][0][0]
  }
  return res


class DataLoader(object):
  def __init__(self, data_dir, batch_size, seq_length):
    self.data_dir = data_dir
    self.batch_size = batch_size
    self.seq_length = seq_length

    self.all_data = self.load_data()

    self.all_batches = []
    self.vid_to_batch_train_x = []
    self.vid_to_batch_train_y = []
    self.vid_to_batch_train_z = []
    self.vid_to_batch_val_x = []
    self.vid_to_batch_val_y = []
    self.vid_to_batch_val_z = []
    for split, vids in enumerate(self.all_data):
      for id, vid in enumerate(vids):
        imfiles = vid['imfiles']
        vidanno = vid['vidanno']
        breaks = None
        if 'break' in vid:
          breaks = vid['break']
        if not os.path.exists(os.path.join(self.data_dir, 'scratch')):
          os.makedirs(os.path.join(self.data_dir, 'scratch'))
        file_to_write = os.path.join(self.data_dir, "scratch/split%d_vid%d_im.pkl" %(split, id))
        if not os.path.exists(file_to_write):
          vidx, vidy, vidz = self.extract(imfiles, vidanno, id, split)
          vbatchesx = []
          vbatchesy = []
          vbatchesz = []
          for k, v in vidx.items():
            if not breaks:
              for l in range(len(v)-seq_length):
                vbatchesx.append(v[l:l+seq_length])
                vbatchesy.append(vidy[k][l:l+seq_length])
                vbatchesz.append(vidz[k][l:l+seq_length])
            else:
              v1x =  v[:breaks]
              v1y = vidy[k][:breaks]
              v1z = vidz[k][:breaks]
              v2x = v[breaks:]
              v2y = vidy[k][breaks:]
              v2z = vidz[k][breaks:]
              for l in range(len(v1x)-seq_length):
                vbatchesx.append(v1x[l:l+seq_length])
                vbatchesy.append(v1y[l:l+seq_length])
                vbatchesz.append(v1z[l:l+seq_length])
              for l in range(len(v2x)-seq_length):
                vbatchesx.append(v2x[l:l+seq_length])
                vbatchesy.append(v2y[l:l+seq_length])
                vbatchesz.append(v2z[l:l+seq_length])
          with open(file_to_write, 'wb') as f:
            pickle.dump({'x': vbatchesx, 'y':vbatchesy, 'z':vbatchesz}, f)
        else:
          with open(file_to_write, 'rb') as f:
            tmp = pickle.load(f)
            vbatchesx = tmp['x']
            vbatchesy = tmp['y']
            vbatchesz = tmp['z']
        if split < 1:
          self.vid_to_batch_train_x.append(vbatchesx)
          self.vid_to_batch_train_y.append(vbatchesy)
          self.vid_to_batch_train_z.append(vbatchesz)
        else:
          self.vid_to_batch_val_x.append(vbatchesx)
          self.vid_to_batch_val_y.append(vbatchesy)
          self.vid_to_batch_val_z.append(vbatchesz)

    all_train_x = self.flatten(self.vid_to_batch_train_x)
    all_train_y = self.flatten(self.vid_to_batch_train_y)
    all_train_z = self.flatten(self.vid_to_batch_train_z)
    all_val_x = self.flatten(self.vid_to_batch_val_x)
    all_val_y = self.flatten(self.vid_to_batch_val_y)
    all_val_z = self.flatten(self.vid_to_batch_val_z)
    
    n_batches_tr = int(np.ceil(float(len(all_train_x)) / batch_size))
    n_batches_va = int(np.ceil(float(len(all_val_x)) / batch_size))
    atx = []
    aty = []
    atz = []
    for i in range(n_batches_tr):
      atx.append(all_train_x[i*batch_size:(i+1)*batch_size])
      aty.append(all_train_y[i*batch_size:(i+1)*batch_size])
      atz.append(all_train_z[i*batch_size:(i+1)*batch_size])
    avx = []
    avy = []
    avz = []
    for i in range(n_batches_va):
      avx.append(all_val_x[i*batch_size:(i+1)*batch_size])
      avy.append(all_val_y[i*batch_size:(i+1)*batch_size])
      avz.append(all_val_z[i*batch_size:(i+1)*batch_size])
    assert len(atx) == len(aty) == len(atz)
    assert len(avx) == len(avy) == len(avz)

    self.all_batches = [[atx, aty, atz], [avx, avy, atz]]
    self.sizes = [len(aty), len(avy)]
    ok("Number of batches in train: %d" % self.sizes[0])
    ok("Number of batches in valid: %d" % self.sizes[1])
    self.batch_idx = [0, 0]

  def flatten(self, lst):
    ret = []
    for i in range(len(lst)):
      for j in range(len(lst[i])):
        if lst[i][j]:
          ret.append(np.array(lst[i][j]))
    return np.array(ret)

  def next_batch(self, type_idx, mode=1):
    # cycle around
    if self.batch_idx[type_idx] >= self.sizes[type_idx]:
      self.batch_idx[type_idx] = 0
    idx = self.batch_idx[type_idx]
    self.batch_idx[type_idx] = self.batch_idx[type_idx] + 1
    rety = self.all_batches[type_idx][mode][idx]
    return self.load_batch(type_idx, idx), rety

  def load_batch(self, type_idx, idx):
    mat = []
    for batch in self.all_batches[type_idx][0][idx]:
      for img_file in batch:
        mat.append(np.load(img_file+'.npy'))
    return np.array(mat)

  def reset_batch_pointer(self, type_idx, batch_idx=None):
    if not batch_idx:
      batch_idx = 0
    self.batch_idx[type_idx] = batch_idx

  def extract(self, imfiles, vidanno, id, split):
    ok('    Extracting {}:{}'.format(split, id))
    # Per person over the whole video
    framedx = defaultdict(list)
    framedy = defaultdict(list)
    framedz = defaultdict(list)
    for fr, im_file in enumerate(imfiles):
      im = imread(im_file)
      # Reading all individuals in the frame
      for j in range(len(vidanno[fr].tid)):
        if not os.path.exists(os.path.join(self.data_dir, 'patches')):
          os.makedirs(os.path.join(self.data_dir, 'patches'))
        pers_save = os.path.join(self.data_dir, 'patches/split%d_vid%d_frame%d_pers%d' %(split, id, fr, j))
        if os.path.exists(pers_save):
          pad_pers = np.load(pers_save+'.npy')
        else:
          (x,y,w,h) = vidanno[fr].bbox[j]
          if x < 0: x = 0
          if y < 0: y = 0
          assert x >= 0, (x, j, i, ni)
          assert y >= 0, y
          assert w > 0, w
          assert h > 0, h
          person = im[y:y+h, x:x+w,:]
          xpad = (0, 0)
          ypad = (0, 0)
          if person.shape[0] < 227:
            xpad = ((227 - person.shape[0])//2, (227 - person.shape[0]+1)//2)
          if person.shape[1] < 227:
            ypad = ((227 - person.shape[1])//2, (227 - person.shape[1]+1)//2)
          if person.shape[0] > 227:
            person = person[:227,:,:] # TODO:
          if person.shape[1] > 227:
            person = person[:,:227,:] # TODO:
          assert person.shape[:2] <= (227, 227), person.shape
          pad_pers  = np.pad(person, (xpad, ypad, (0,0)), 'constant')
          act = vidanno[fr].actions[j]
          clab = vidanno[fr].clabel
          assert not os.path.exists(pers_save)
          np.save(pers_save, pad_pers)

        framedx[j].append(pers_save)
        framedy[j].append(act)
        framedz[j].append(clab)
    return framedx, framedy, framedz

  def load_data(self):
    ok('   Loading Data...')
    dat_path = os.path.join(self.data_dir, 'all_anno.pkl')
    if os.path.exists(dat_path):
      with open(dat_path, 'rb') as f:
        return pickle.load(f)

    all_data = []
    train_data = []
    val_data = []
    for ni in range(1,34):
      img_dir = os.path.join(self.data_dir, 'images/seq%02d' % ni)
      imfiles = sorted(glob.glob(os.path.join(img_dir, '*.jpg')))
      anno = annomat(os.path.join(self.data_dir, 'annotations/anno%02d.mat' % ni))
      vidanno = get_video_annotations(anno)

      assert len(vidanno) == len(imfiles), (len(vidanno), len(imfiles))
      n_frames = len(imfiles)
      n_val_frames = int(n_frames*0.2)
      train_imfiles = imfiles[:n_frames//2] + imfiles[n_frames//2+n_val_frames:]
      val_imfiles = imfiles[n_frames//2:n_frames//2+n_val_frames]
      train_vidanno = vidanno[:n_frames//2] + vidanno[n_frames//2+n_val_frames:]
      val_vidanno = vidanno[n_frames//2:n_frames//2+n_val_frames]
      lut_train = {
        'imfiles': train_imfiles,
        'vidanno': train_vidanno,
        'break': n_frames//2
      }
      lut_val = {
        'imfiles': val_imfiles,
        'vidanno': val_vidanno,
      }
      train_data.append(lut_train)
      val_data.append(lut_val)
    all_data = [train_data, val_data]
    with open(dat_path, 'wb') as f:
      pickle.dump(all_data, f)
    return all_data
